﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using AuthSolution.Shared.Identify.Models;
using AuthSolution.Shared.Identify.Tests.Extensions;
using AuthSolution.Shared.Identify.Utility;
using Moq;
using Moq.Protected;
using Xunit;

namespace AuthSolution.Shared.Identify.Tests.Utility
{
    [CollectionDefinition("HttpClientHelper", DisableParallelization = true)]
    [Collection("HttpRequestMessage")]
    public class HttpClientHelperTests
    {
        private const string BaseUrl = "http://fake.url";

        public static IEnumerable<object?[]> IdSessionCreateDtoData =>
            new List<object?[]>
            {
                new object?[] { null, null, null, null, null, null, null, null, null, null, },
                new object?[]
                {
                    new List<string>(),
                    "Fake Language",
                    "Fake Flow",
                    new List<string>(),
                    new IdSessionRedirectSettingsDto(),
                    new IdSessionIframeSettingsDto(),
                    "Fake ExternalReference",
                    "Fake DepartmentId",
                    new IdPrefilledInputDto(),
                    new IdSessionUiDto(),
                },
                new object?[]
                {
                    new List<string>(new[] { "FakeProvider" }),
                    "Fake Language",
                    "Fake Flow",
                    new List<string>(new[] { "FakeInclude" }),
                    new IdSessionRedirectSettingsDto(),
                    new IdSessionIframeSettingsDto(),
                    "Fake ExternalReference",
                    "Fake DepartmentId",
                    new IdPrefilledInputDto(),
                    new IdSessionUiDto(),
                },
            };

        public static IEnumerable<object?[]> IdSessionGetDtoData =>
            new List<object?[]>
            {
                new object?[] { null, null, null, null, null, null, null, null, null, null, null, null, null, },
                new object?[]
                {
                    "Fake Id",
                    "Fake Url",
                    "Fake Status",
                    "Fake Provider",
                    new IdSessionIdentityDto(),
                    new IdSessionEnvironmentDto(),
                    new List<string>(),
                    "Fake Language",
                    "Fake Flow",
                    new List<string>(),
                    new IdSessionRedirectSettingsDto(),
                    "Fake ExternalReference",
                    new IdPrefilledInputDto(),
                },
                new object?[]
                {
                    "Fake Id",
                    "Fake Url",
                    "Fake Status",
                    "Fake Provider",
                    new IdSessionIdentityDto(),
                    new IdSessionEnvironmentDto(),
                    new List<string>(new[] { "FakeProvider" }),
                    "Fake Language",
                    "Fake Flow",
                    new List<string>(new[] { "FakeInclude" }),
                    new IdSessionRedirectSettingsDto(),
                    "Fake ExternalReference",
                    new IdPrefilledInputDto(),
                },
            };

        public static IEnumerable<object?[]> IdSessionIframeSettingsDtoData =>
            new List<object?[]>
            {
                new object?[] { null, null, },
                new object?[]
                {
                    new List<string>(),
                    "Fake PostMessageTargetOrigin",
                },
                new object?[]
                {
                    new List<string>(new[] { "FakeParentDomain" }),
                    "Fake PostMessageTargetOrigin",
                },
            };

        [Fact]
        public void NullClient_SetsNewClient()
        {
            // Arrange
            HttpClientHelper.HttpClientLifeSpan = TimeSpan.FromMinutes(1);
            HttpClientHelper.Client = null!;

            // Act & Assert
            Assert.NotNull(HttpClientHelper.Client);
        }

        [Fact]
        public void ExpiredLifeSpan_SetsNewClient()
        {
            // Arrange
            var oldTimeSpan = TimeSpan.FromSeconds(HttpClientHelper.HttpClientLifeSpan.TotalSeconds);
            HttpClientHelper.HttpClientLifeSpan = TimeSpan.FromSeconds(1);
            HttpClientHelper.Client = new HttpClient();
            var previousHashCode = HttpClientHelper.Client.GetHashCode();
            Thread.Sleep(3000);
            HttpClientHelper.HttpClientLifeSpan = TimeSpan.FromSeconds(oldTimeSpan.TotalSeconds);

            // Act & Assert
            var newHashCode = HttpClientHelper.Client.GetHashCode();
            Assert.NotEqual(newHashCode, previousHashCode);
        }

        [Fact]
        public void ClientDisposed_ThrowsObjectDisposedException()
        {
            // Arrange
            HttpClientHelper.Client = new HttpClient();
            HttpClientHelper.Client.Dispose();

            // Act & Assert
            Assert.Throws<ObjectDisposedException>(() => HttpClientHelper.Client!);
        }

        [Fact]
        public void LifeSpanSet()
        {
            // Arrange
            var lifeSpanInMinutes = new Random().Next(3, 8);
            HttpClientHelper.HttpClientLifeSpan = TimeSpan.FromMinutes(lifeSpanInMinutes);

            // Act & Assert
            Assert.Equal(TimeSpan.FromMinutes(lifeSpanInMinutes), HttpClientHelper.HttpClientLifeSpan);
        }

        [Theory]
        [InlineData(null, null, null, null, null)]
        [InlineData("Fake Type", true, "Fake Description", "Fake LogoUrl", "Fake Name")]
        [InlineData("Fake Type", false, "Fake Description", "Fake LogoUrl", "Fake Name")]
        public async Task IdIdentityProviderDto_Response(string? type, bool? canIframe, string? description, string? logoUrl, string? name)
        {
            // Arrange
            var content = new IdIdentityProviderDto()
            {
                Type = type,
                CanIframe = canIframe,
                Description = description,
                LogoUrl = logoUrl,
                Name = name,
            };

            // Act
            var response = await Request(content);

            // Assert
            Assert.NotNull(response);
            AssertExtensions.Equivalent(content, response);
        }

        [Theory]
        [InlineData(null, null, null, null)]
        [InlineData("Fake NIN", "Fake MobileNumber", "Fake DateOfBirth", "Fake Username")]
        public async Task IdPrefilledInputDto_Response(string? nin, string? mobileNumber, string? dateOfBirth, string? username)
        {
            // Arrange
            var content = new IdPrefilledInputDto()
            {
                Nin = nin,
                MobileNumber = mobileNumber,
                DateOfBirth = dateOfBirth,
                Username = username,
            };

            // Act
            var response = await Request(content);

            // Assert
            Assert.NotNull(response);
            AssertExtensions.Equivalent(content, response);
        }

        [Theory]
        [MemberData(nameof(IdSessionCreateDtoData))]
        public async Task IdSessionCreateDto_Response(
            List<string>? allowedProviders,
            string? language,
            string? flow,
            List<string>? include,
            IdSessionRedirectSettingsDto? redirectSettings,
            IdSessionIframeSettingsDto? iframeSettings,
            string? externalReference,
            string? departmentId,
            IdPrefilledInputDto? prefilledInput,
            IdSessionUiDto? ui)
        {
            // Arrange
            var content = new IdSessionCreateDto()
            {
                AllowedProviders = allowedProviders,
                Language = language,
                Flow = flow,
                Include = include,
                RedirectSettings = redirectSettings,
                IframeSettings = iframeSettings,
                ExternalReference = externalReference,
                DepartmentId = departmentId,
                PrefilledInput = prefilledInput,
                Ui = ui,
            };

            // Act
            var response = await Request(content);

            // Assert
            Assert.NotNull(response);
            AssertExtensions.Equivalent(content, response);
        }

        [Theory]
        [InlineData(null, null)]
        [InlineData("Fake IpAddress", "Fake UserAgent")]
        public async Task IdSessionEnvironmentDto_Response(string? ipAddress, string? userAgent)
        {
            // Arrange
            var content = new IdSessionEnvironmentDto()
            {
                IpAddress = ipAddress,
                UserAgent = userAgent,
            };

            // Act
            var response = await Request(content);

            // Assert
            Assert.NotNull(response);
            AssertExtensions.Equivalent(content, response);
        }

        [Theory]
        [MemberData(nameof(IdSessionGetDtoData))]
        public async Task IdSessionGetDto_Response(
            string? id,
            string? url,
            string? status,
            string? provider,
            IdSessionIdentityDto? identity,
            IdSessionEnvironmentDto? environment,
            List<string>? providers,
            string? language,
            string? flow,
            List<string>? include,
            IdSessionRedirectSettingsDto? redirectSettings,
            string? externalReference,
            IdPrefilledInputDto? prefilledInput)
        {
            // Arrange
            var content = new IdSessionGetDto()
            {
                Id = id,
                Url = url,
                Status = status,
                Provider = provider,
                Identity = identity,
                Environment = environment,
                Providers = providers,
                Language = language,
                Flow = flow,
                Include = include,
                IdSessionRedirectSettings = redirectSettings,
                ExternalReference = externalReference,
                PrefilledInput = prefilledInput,
            };

            // Act
            var response = await Request(content);

            // Assert
            Assert.NotNull(response);
            AssertExtensions.Equivalent(content, response);
        }

        [Theory]
        [InlineData(null, null, null, null, null, null, null, null)]
        [InlineData("Fake ProviderId", "Fake FullName", "Fake FirstName", "Fake MiddleName", "Fake LastName", "Fake DateOfBirth", "Fake Nin", "Fake PhoneNumber")]
        public async Task IdSessionIdentityDto_Response(
            string? providerId,
            string? fullName,
            string? firstName,
            string? middleName,
            string? lastName,
            string? dateOfBirth,
            string? nin,
            string? phoneNumber)
        {
            // Arrange
            var content = new IdSessionIdentityDto()
            {
                ProviderId = providerId,
                FullName = fullName,
                FirstName = firstName,
                MiddleName = middleName,
                LastName = lastName,
                DateOfBirth = dateOfBirth,
                Nin = nin,
                PhoneNumber = phoneNumber,
            };

            // Act
            var response = await Request(content);

            // Assert
            Assert.NotNull(response);
            AssertExtensions.Equivalent(content, response);
        }

        [Theory]
        [MemberData(nameof(IdSessionIframeSettingsDtoData))]
        public async Task IdSessionIframeSettingsDto_Response(List<string>? parentDomains, string? postMessageTargetOrigin)
        {
            // Arrange
            var content = new IdSessionIframeSettingsDto()
            {
                ParentDomains = parentDomains,
                PostMessageTargetOrigin = postMessageTargetOrigin,
            };

            // Act
            var response = await Request(content);

            // Assert
            Assert.NotNull(response);
            AssertExtensions.Equivalent(content, response);
        }

        [Theory]
        [InlineData(null, null, null)]
        [InlineData("Fake SuccessUrl", "Fake AbortUrl", "Fake ErrorUrl")]
        public async Task IdSessionRedirectSettingsDto_Response(string? successUrl, string? abortUrl, string? errorUrl)
        {
            // Arrange
            var content = new IdSessionRedirectSettingsDto()
            {
                SuccessUrl = successUrl,
                AbortUrl = abortUrl,
                ErrorUrl = errorUrl,
            };

            // Act
            var response = await Request(content);

            // Assert
            Assert.NotNull(response);
            AssertExtensions.Equivalent(content, response);
        }

        [Theory]
        [InlineData(null, null)]
        [InlineData(true, true)]
        [InlineData(false, false)]
        [InlineData(true, false)]
        [InlineData(false, true)]
        public async Task IdSessionUiDto_Response(bool? hideProviderHeader, bool? hideFooter)
        {
            // Arrange
            var content = new IdSessionUiDto()
            {
                HideProviderHeader = hideProviderHeader,
                HideFooter = hideFooter,
            };

            // Act
            var response = await Request(content);

            // Assert
            Assert.NotNull(response);
            AssertExtensions.Equivalent(content, response);
        }

        private async Task<T> Request<T>(T content)
        {
            var handlerMock = new Mock<HttpMessageHandler>();
            string serializedContent = JsonSerializer.Serialize(content);
            var rawResponse = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(serializedContent),
            };
            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(rawResponse);
            HttpClientHelper.Client = new HttpClient(handlerMock.Object);

            using var req = new HttpRequestMessage(HttpMethod.Get, BaseUrl);
            HttpResponseMessage response = await HttpClientHelper.Client.SendAsync(req);
            string responseContent = await response.Content.ReadAsStringAsync();

            return JsonSerializer.Deserialize<T>(responseContent);
        }
    }
}
