﻿using System.Text.Json;
using Xunit;
using Xunit.Sdk;

namespace AuthSolution.Shared.Identify.Tests.Extensions
{
    /// <summary>
    /// Assert extensions.
    /// </summary>
    public static class AssertExtensions
    {
        /// <summary>
        /// Verifies that two sequences are equivalent, using a default comparer.
        /// </summary>
        /// <param name="expected">The expected value.</param>
        /// <param name="actual">The value to be compared against.</param>
        /// <typeparam name="T">The type of the objects to be compared.</typeparam>
        /// <exception cref="EqualException">Thrown when the objects are not equal.</exception>
        public static void Equivalent<T>(T expected, T actual)
        {
            string? serializedExpected = expected == null ? null : JsonSerializer.Serialize(expected);
            string? serializedActual = actual == null ? null : JsonSerializer.Serialize(actual);

            Assert.Equal(serializedExpected, serializedActual);
        }
    }
}