﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using AuthSolution.Shared.Identify.Abstractions.Config;
using AuthSolution.Shared.Identify.Models;
using AuthSolution.Shared.Identify.Tests.Extensions;
using AuthSolution.Shared.Identify.Utility;
using AutoFixture;
using Idfy.Core.Security.Http.Abstractions;
using Moq;
using Moq.Protected;
using Serilog;
using Xunit;

namespace AuthSolution.Shared.Identify.Tests
{
    [Collection("HttpRequestMessage")]
    public class IdApiServiceTests
    {
        private const string CredentialsClientId = "fake";
        private const string CredentialsClientSecret = "fake";
        private const string CredentialsIdserverBaseUrl = "http://idserver.fake.url/";
        private const string IdentifyApiBaseUrl = "http://fake.url";

        private readonly Fixture _fixture = new Fixture();

        [Theory]
        [InlineData("GET")]
        [InlineData("POST")]
        public async Task DoRequest_WhenInvoked_HasBillingHeader(string httpMethod)
        {
            // Arrange
            var requestMessage = new HttpRequestMessage();
            IdentifyApiConfig identifyApiConfig = GetIdentifyApiConfig();
            IdApiService idApiService = CreateIdApiService(identifyApiConfig, (r, ct) => { requestMessage = r; });

            // Act
            HttpResponseMessage? result = await idApiService.DoRequest(IdentifyApiBaseUrl, new HttpMethod(httpMethod), null, false);

            // Assert
            Assert.True(requestMessage.Headers.Contains("x-signicat-context"));
            Assert.Equal("team-auth-oidc", requestMessage.Headers.GetValues("x-signicat-context").Single());
            Assert.NotNull(identifyApiConfig.Credentials);
            Assert.Equal(CredentialsClientId, identifyApiConfig.Credentials!.ClientId);
            Assert.Equal(CredentialsClientSecret, identifyApiConfig.Credentials!.ClientSecret);
            Assert.Equal(CredentialsIdserverBaseUrl, identifyApiConfig.Credentials!.IdserverBaseUrl);
            Assert.Equal(IdentifyApiBaseUrl, identifyApiConfig.BaseUrl);
        }

        [Theory]
        [InlineData(HttpStatusCode.BadRequest)]
        [InlineData(HttpStatusCode.Forbidden)]
        [InlineData(HttpStatusCode.BadGateway)]
        public async Task DoRequest_WhenInsuccessHttpCode_ThrowsInvalidOperationException(HttpStatusCode httpStatusCode)
        {
            // Arrange
            IdentifyApiConfig identifyApiConfig = GetIdentifyApiConfig();
            IdApiService idApiService = CreateIdApiService(identifyApiConfig, httpStatusCode);

            // Act & Assert
            await Assert.ThrowsAsync<InvalidOperationException>(
                () => idApiService.DoRequest(
                    path: IdentifyApiBaseUrl,
                    method: new HttpMethod("GET"),
                    requestFn: null,
                    applyCredentials: false));
        }

        [Fact]
        public async Task DoRequest_WhenExceptionOccursOnContentRead_ThrowsInvalidOperationExceptionWithInnerException()
        {
            // Arrange
            IdentifyApiConfig identifyApiConfig = GetIdentifyApiConfig();
            IdApiService idApiService = CreateIdApiService(identifyApiConfig, HttpStatusCode.BadRequest, null);

            // Act & Assert
            var exception = await Assert.ThrowsAsync<InvalidOperationException>(
                () => idApiService.DoRequest(
                    path: IdentifyApiBaseUrl,
                    method: new HttpMethod("GET"),
                    requestFn: null,
                    applyCredentials: false));
            Assert.NotNull(exception.InnerException);
        }

        [Fact]
        public void NoBaseUrl_ThrowArgumentNullException()
        {
            // Arrange
            var config = new IdentifyApiConfig
            {
                BaseUrl = null,
                Credentials = new IdfyCredentialsConfig()
                {
                    ClientId = CredentialsClientId,
                    ClientSecret = CredentialsClientSecret,
                    IdserverBaseUrl = CredentialsIdserverBaseUrl,
                },
            };

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => new IdApiService(config, Log.Logger, Mock.Of<IHttpRequestCredentialsApplier>()));
        }

        [Fact]
        public void NoCredentials_ThrowArgumentNullException()
        {
            // Arrange
            var config = new IdentifyApiConfig
            {
                BaseUrl = IdentifyApiBaseUrl,
                Credentials = null,
            };

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => new IdApiService(config, Log.Logger, Mock.Of<IHttpRequestCredentialsApplier>()));
        }

        [Theory]
        [InlineData("")]
        [InlineData("http://test.url")]
        [InlineData("http://test.url/content")]
        [InlineData("http://test.url/content?param=1")]
        public void FixUrl_Should_Not_Change(string baseUrl)
        {
            // Arrange
            var config = new IdentifyApiConfig
            {
                BaseUrl = baseUrl,
                Credentials = new IdfyCredentialsConfig()
                {
                    ClientId = CredentialsClientId,
                    ClientSecret = CredentialsClientSecret,
                    IdserverBaseUrl = CredentialsIdserverBaseUrl,
                },
            };

            // Act
            var idApi = new IdApiService(config, Log.Logger, Mock.Of<IHttpRequestCredentialsApplier>());

            // Assert
            Assert.Equal(baseUrl, config.BaseUrl);
        }

        [Theory]
        [InlineData("/")]
        [InlineData("http://test.url/")]
        [InlineData("http://test.url/content/")]
        public void FixUrl_Should_Remove_Forward_Slash(string baseUrl)
        {
            // Arrange
            var config = new IdentifyApiConfig
            {
                BaseUrl = baseUrl,
                Credentials = new IdfyCredentialsConfig()
                {
                    ClientId = CredentialsClientId,
                    ClientSecret = CredentialsClientSecret,
                    IdserverBaseUrl = CredentialsIdserverBaseUrl,
                },
            };

            // Act
            var baseUrlChanged = baseUrl.Substring(0, baseUrl.Length - 1);
            var idApi = new IdApiService(config, Log.Logger, Mock.Of<IHttpRequestCredentialsApplier>());

            // Assert
            Assert.Equal(baseUrlChanged, config.BaseUrl);
        }

        [Fact]
        public async Task GetIdentificationResponse_WhenInvoked_ReturnsIdSessionGetDtoSuccessfully()
        {
            // Arrange
            IdSessionGetDto expectedDtoResult = _fixture.Create<IdSessionGetDto>();

            IdentifyApiConfig identifyApiConfig = GetIdentifyApiConfig();
            IdApiService idApiService = CreateIdApiService(identifyApiConfig, contentResponse: JsonSerializer.Serialize(expectedDtoResult));

            // Act
            IdSessionGetDto result = await idApiService.GetIdentificationResponse(sessionId: "session_fake_id", accountId: "account_fake_id");

            // Assert
            AssertExtensions.Equivalent(expectedDtoResult, result);
        }

        [Fact]
        public async Task CreateIdentificationRequest_WhenInvoked_ReturnsIdSessionGetDtoSuccessfully()
        {
            // Arrange
            IdSessionGetDto expectedDtoResult = _fixture.Create<IdSessionGetDto>();

            IdentifyApiConfig identifyApiConfig = GetIdentifyApiConfig();
            IdApiService idApiService = CreateIdApiService(identifyApiConfig, contentResponse: JsonSerializer.Serialize(expectedDtoResult));

            // Act
            IdSessionGetDto result = await idApiService.CreateIdentificationRequest(_fixture.Create<IdSessionCreateDto>(), accountId: "account_fake_id");

            // Assert
            AssertExtensions.Equivalent(expectedDtoResult, result);
        }

        [Fact]
        public async Task ListIdentityProviders_WhenInvoked_ReturnsListOfIdIdentityProviderDtoSuccessfully()
        {
            // Arrange
            List<IdIdentityProviderDto> expectedDtoResult = _fixture.CreateMany<IdIdentityProviderDto>().ToList();

            IdentifyApiConfig identifyApiConfig = GetIdentifyApiConfig();
            IdApiService idApiService = CreateIdApiService(identifyApiConfig, contentResponse: JsonSerializer.Serialize(expectedDtoResult));

            // Act
            List<IdIdentityProviderDto> result = await idApiService.ListIdentityProviders(accountId: "account_fake_id");

            // Assert
            AssertExtensions.Equivalent(expectedDtoResult, result);
        }

        private static IdApiService CreateIdApiService(
            IdentifyApiConfig identifyApiConfig,
            HttpStatusCode httpStatusCode = HttpStatusCode.OK,
            string? contentResponse = "")
        {
            return CreateIdApiService(identifyApiConfig, null, httpStatusCode, contentResponse);
        }

        private static IdApiService CreateIdApiService(
            IdentifyApiConfig identifyApiConfig,
            Action<HttpRequestMessage, CancellationToken>? requestMessageCallback,
            HttpStatusCode httpStatusCode = HttpStatusCode.OK,
            string? contentResponse = "")
        {
            if (requestMessageCallback == null)
            {
                requestMessageCallback = (m, c) => { };
            }

            var handlerMock = new Mock<HttpMessageHandler>();
            var response = new HttpResponseMessage
            {
                StatusCode = httpStatusCode,
                Content = contentResponse == null ? null : new StringContent(contentResponse),
                RequestMessage = new HttpRequestMessage { RequestUri = new Uri(IdentifyApiBaseUrl) },
            };
            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .Callback(requestMessageCallback)
                .ReturnsAsync(response);
            HttpClientHelper.Client = new HttpClient(handlerMock.Object);

            var idApiService = new IdApiService(identifyApiConfig, Log.Logger, Mock.Of<IHttpRequestCredentialsApplier>());
            return idApiService;
        }

        private static IdentifyApiConfig GetIdentifyApiConfig()
        {
            return new IdentifyApiConfig
            {
                BaseUrl = IdentifyApiBaseUrl,
                Credentials = new IdfyCredentialsConfig()
                {
                    ClientId = CredentialsClientId,
                    ClientSecret = CredentialsClientSecret,
                    IdserverBaseUrl = CredentialsIdserverBaseUrl,
                },
            };
        }
    }
}
