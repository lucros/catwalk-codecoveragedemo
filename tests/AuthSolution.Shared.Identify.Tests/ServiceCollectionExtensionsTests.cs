﻿using System;
using System.IO;
using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Serilog;
using Xunit;

namespace AuthSolution.Shared.Identify.Tests
{
    public class ServiceCollectionExtensionsTests
    {
        private const string CredentialsClientId = "FakeClientId";
        private const string CredentialsClientSecret = "FakeClientSecret";
        private const string CredentialsIdserverBaseUrl = "http://idserver.fake.url/";
        private const string IdentifyApiBaseUrl = "http://fake.url";

        [Fact]
        public void AddIdApiServices_ConfigIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            var serviceCollection = Mock.Of<IServiceCollection>();

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => serviceCollection.AddIdApiServices(config: null!));
        }

        [Fact]
        public void AddIdApiServices_Config_Sections_Credentials_And_IdApi_Not_Present_ThrowArgumentNullException()
        {
            // Arrange
            IConfiguration config = CreateConfig(new
            {
                SomeSetting = "Default",
            });
            IServiceCollection serviceCollection = new ServiceCollection();
            var logger = new Mock<ILogger>();

            // Act
            serviceCollection.AddSingleton<ILogger>(ctx => logger.Object);
            serviceCollection.AddIdApiServices(config: config);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // Assert
            Assert.Equal(4, serviceCollection.Count);
            Assert.Throws<ArgumentNullException>(() => serviceProvider.GetRequiredService<IIdApiService>());
        }

        [Fact]
        public void AddIdApiServices_Config_Section_IdApi_Not_Present_ThrowArgumentNullException()
        {
            // Arrange
            IConfiguration config = CreateConfig(new
            {
                Idfy = new
                {
                    Credentials = new
                    {
                        ClientId = CredentialsClientId,
                        ClientSecret = CredentialsClientSecret,
                        IdserverBaseUrl = CredentialsIdserverBaseUrl,
                    },
                },
            });
            IServiceCollection serviceCollection = new ServiceCollection();
            var logger = new Mock<ILogger>();

            // Act
            serviceCollection.AddSingleton<ILogger>(ctx => logger.Object);
            serviceCollection.AddIdApiServices(config: config);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // Assert
            Assert.Equal(4, serviceCollection.Count);
            Assert.Throws<ArgumentNullException>(() => serviceProvider.GetRequiredService<IIdApiService>());
        }

        [Fact]
        public void AddIdApiServices_Config_Section_IdApi_BaseUrl_Not_Present_ThrowArgumentNullException()
        {
            // Arrange
            IConfiguration config = CreateConfig(new
            {
                Idfy = new
                {
                    Credentials = new
                    {
                        ClientId = CredentialsClientId,
                        ClientSecret = CredentialsClientSecret,
                        IdserverBaseUrl = CredentialsIdserverBaseUrl,
                    },
                    IdApi = new
                    {
                        Credentials = new
                        {
                            ClientId = "CustomFakeClientId",
                            ClientSecret = "CustomFakeSecret",
                            IdserverBaseUrl = "CustomFakeIdserverBaseUrl",
                        },
                    },
                },
            });
            IServiceCollection serviceCollection = new ServiceCollection();
            var logger = new Mock<ILogger>();

            // Act
            serviceCollection.AddSingleton<ILogger>(ctx => logger.Object);
            serviceCollection.AddIdApiServices(config: config);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // Assert
            Assert.Equal(4, serviceCollection.Count);
            Assert.Throws<ArgumentNullException>(() => serviceProvider.GetRequiredService<IIdApiService>());
        }

        [Theory]
        [InlineData(null, null, null)]
        [InlineData("CustomFakeClientId", null, null)]
        [InlineData(null, "CustomFakeSecret", null)]
        [InlineData(null, null, "CustomFakeIdserverBaseUrl")]
        [InlineData("CustomFakeClientId", "CustomFakeSecret", "CustomFakeIdserverBaseUrl")]
        public void AddIdApiServices_Registered_With_DefaultCredentials_When_CustomNotPresent(string? customClientId, string? customClientSecret, string? customIdserverBaseUrl)
        {
            // Arrange
            IConfiguration config = CreateConfig(new
            {
                Idfy = new
                {
                    Credentials = new
                    {
                        ClientId = CredentialsClientId,
                        ClientSecret = CredentialsClientSecret,
                        IdserverBaseUrl = CredentialsIdserverBaseUrl,
                    },
                    IdApi = new
                    {
                        BaseUrl = IdentifyApiBaseUrl,
                        Credentials = new
                        {
                            ClientId = customClientId,
                            ClientSecret = customClientSecret,
                            IdserverBaseUrl = customIdserverBaseUrl,
                        },
                    },
                },
            });
            IServiceCollection serviceCollection = new ServiceCollection();
            var logger = new Mock<ILogger>();

            // Act
            serviceCollection.AddSingleton<ILogger>(ctx => logger.Object);
            serviceCollection.AddIdApiServices(config: config);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // Assert
            var serviceInstance = serviceProvider.GetRequiredService<IIdApiService>();
            Assert.Equal(4, serviceCollection.Count);
            Assert.NotNull(serviceInstance);
            Assert.Equal(typeof(IIdApiService), serviceCollection[serviceCollection.Count - 1].ServiceType);
            Assert.Equal(typeof(IdApiService), serviceInstance.GetType());
        }

        private static IConfiguration CreateConfig(dynamic jsonObject)
        {
            string serializedObject = JsonSerializer.Serialize(jsonObject);
            return CreateConfig(serializedObject);
        }

        private static IConfiguration CreateConfig(string jsonConfiguration)
        {
            var configBuilder = new ConfigurationBuilder();
            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(jsonConfiguration));
            configBuilder.AddJsonStream(stream);
            return configBuilder.Build();
        }
    }
}
