﻿/*
 * Originally copied from src/Idfy.IdentityServer.Abstractions/Config/IdentifyApiConfig.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */
namespace AuthSolution.Shared.Identify.Abstractions.Config
{
    public class IdentifyApiConfig
    {
        public IdfyCredentialsConfig? Credentials { get; set; }

        public string? BaseUrl { get; set; }
    }
}
