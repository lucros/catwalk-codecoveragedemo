﻿/*
 * Originally copied from src/Idfy.IdentityServer.Abstractions/Config/IdfyCredentialsConfig.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */
namespace AuthSolution.Shared.Identify.Abstractions.Config
{
    public class IdfyCredentialsConfig
    {
        public string? IdserverBaseUrl { get; set; }

        public string? ClientId { get; set; }

        public string? ClientSecret { get; set; }
    }
}
