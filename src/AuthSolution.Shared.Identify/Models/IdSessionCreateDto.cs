﻿/*
 * Originally copied from src/Idfy.IdentityServer.Services.Idfy/Id/IdSessionCreateDto.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace AuthSolution.Shared.Identify.Models
{
    [SuppressMessage("Microsoft.Design", "CA2227", Justification = "Model class")]
    public class IdSessionCreateDto
    {
        public List<string>? AllowedProviders { get; set; }

        public string? Language { get; set; }

        public string? Flow { get; set; }

        public List<string>? Include { get; set; }

        public IdSessionRedirectSettingsDto? RedirectSettings { get; set; }

        public IdSessionIframeSettingsDto? IframeSettings { get; set; }

        public string? ExternalReference { get; set; }

        public string? DepartmentId { get; set; }

        public IdPrefilledInputDto? PrefilledInput { get; set; }

        public IdSessionUiDto? Ui { get; set; }
    }
}
