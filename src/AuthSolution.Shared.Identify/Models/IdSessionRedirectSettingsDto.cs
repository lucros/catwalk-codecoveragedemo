﻿/*
 * Originally copied from src/Idfy.IdentityServer.Services.Idfy/Id/IdSessionRedirectSettingsDto.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */
namespace AuthSolution.Shared.Identify.Models
{
    public class IdSessionRedirectSettingsDto
    {
        public string? SuccessUrl { get; set; }

        public string? AbortUrl { get; set; }

        public string? ErrorUrl { get; set; }
    }
}
