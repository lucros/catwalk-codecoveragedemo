﻿/*
 * Originally copied from src/Idfy.IdentityServer.Services.Idfy/Id/IdSessionGetDto.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */
namespace AuthSolution.Shared.Identify.Models
{
    public class IdSessionIdentityDto
    {
        public string? ProviderId { get; set; }

        public string? FullName { get; set; }

        public string? FirstName { get; set; }

        public string? MiddleName { get; set; }

        public string? LastName { get; set; }

        public string? DateOfBirth { get; set; }

        public string? Nin { get; set; }

        public string? PhoneNumber { get; set; }
    }
}
