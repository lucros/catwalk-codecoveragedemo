﻿/*
 * Originally copied from src/Idfy.IdentityServer.Services.Idfy/Id/IdSessionGetDto.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace AuthSolution.Shared.Identify.Models
{
    [SuppressMessage("Microsoft.Design", "CA2227", Justification = "Model class")]
    public class IdSessionGetDto
    {
        public string? Id { get; set; }

        public string? Url { get; set; }

        public string? Status { get; set; }

        public string? Provider { get; set; }

        public IdSessionIdentityDto? Identity { get; set; }

        public IdSessionEnvironmentDto? Environment { get; set; }

        public List<string>? Providers { get; set; }

        public string? Language { get; set; }

        public string? Flow { get; set; }

        public List<string>? Include { get; set; }

        public IdSessionRedirectSettingsDto? IdSessionRedirectSettings { get; set; }

        public string? ExternalReference { get; set; }

        public IdPrefilledInputDto? PrefilledInput { get; set; }
    }
}
