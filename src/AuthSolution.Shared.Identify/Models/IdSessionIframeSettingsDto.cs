﻿/*
 * Originally copied from src/Idfy.IdentityServer.Services.Idfy/Id/IdSessionIframeSettingsDto.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace AuthSolution.Shared.Identify.Models
{
    [SuppressMessage("Microsoft.Design", "CA2227", Justification = "Model class")]
    public class IdSessionIframeSettingsDto
    {
        public List<string>? ParentDomains { get; set; }

        public string? PostMessageTargetOrigin { get; set; }
    }
}
