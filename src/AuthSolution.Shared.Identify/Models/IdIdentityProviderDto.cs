﻿/*
 * Originally copied from src/Idfy.IdentityServer.Services.Idfy/Id/IdIdentityProviderDto.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace AuthSolution.Shared.Identify.Models
{
    [SuppressMessage("Microsoft.Design", "CA2227", Justification = "Model class")]
    public class IdIdentityProviderDto
    {
        public string? Type { get; set; }

        public string? Name { get; set; }

        public string? Description { get; set; }

        public string? LogoUrl { get; set; }

        public bool? CanIframe { get; set; }

        public List<string>? SupportedScopes { get; set; }
    }
}
