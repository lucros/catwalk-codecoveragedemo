﻿/*
 * Originally copied from src/Idfy.IdentityServer.Services.Idfy/Id/IdSessionUiDto.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */
namespace AuthSolution.Shared.Identify.Models
{
    public class IdSessionUiDto
    {
        public bool? HideProviderHeader { get; set; }

        public bool? HideFooter { get; set; }
    }
}
