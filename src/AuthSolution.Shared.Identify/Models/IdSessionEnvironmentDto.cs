﻿/*
 * Originally copied from src/Idfy.IdentityServer.Services.Idfy/Id/IdSessionGetDto.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */
namespace AuthSolution.Shared.Identify.Models
{
    public class IdSessionEnvironmentDto
    {
        public string? UserAgent { get; set; }

        public string? IpAddress { get; set; }
    }
}
