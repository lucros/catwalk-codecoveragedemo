﻿/*
 * Originally copied from src/Idfy.IdentityServer.Services.Idfy/ServiceCollectionExtensions.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */

using System.Collections.Generic;
using ArgumentValidator;
using AuthSolution.Shared.Identify.Abstractions.Config;
using Idfy.Core.Security.Http.Abstractions;
using Idfy.Core.Security.Http.Bearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace AuthSolution.Shared.Identify
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddIdApiServices(this IServiceCollection services, IConfiguration config)
        {
            Throw.IfNull(config, nameof(config));
            var defaultCredentials = new IdfyCredentialsConfig();
            config.GetSection("idfy:credentials").Bind(defaultCredentials);

            var idConfig = new IdentifyApiConfig();
            config.GetSection("idfy:idApi").Bind(idConfig);

            if (idConfig.Credentials == null)
            {
                idConfig.Credentials = defaultCredentials;
            }
            else
            {
                ApplyDefaultCredentials(defaultCredentials, idConfig.Credentials);
            }

            services.AddTransient(provider => idConfig);
            services.AddTransient<IHttpRequestCredentialsApplier>(ctx => new BearerTokenHttpRequestCredentialsApplier(
                new IdfyClientCredentialsBearerTokenProvider(
                    new IdfyClientCredentialsBearerTokenProviderOptions
                    {
                        ClientId = idConfig.Credentials!.ClientId,
                        ClientSecret = idConfig.Credentials.ClientSecret,
                        Authority = idConfig.Credentials.IdserverBaseUrl,
                        Scopes = new List<string>() { "app" },
                    },
                    ctx.GetRequiredService<ILogger>()
                )
            ));
            services.AddSingleton<IIdApiService, IdApiService>();

            return services;
        }

        private static void ApplyDefaultCredentials(IdfyCredentialsConfig defaults, IdfyCredentialsConfig target)
        {
            target.IdserverBaseUrl = string.IsNullOrEmpty(target.IdserverBaseUrl) ? defaults.IdserverBaseUrl : target.IdserverBaseUrl;
            target.ClientId = string.IsNullOrEmpty(target.ClientId) ? defaults.ClientId : target.ClientId;
            target.ClientSecret = string.IsNullOrEmpty(target.ClientSecret) ? defaults.ClientSecret : target.ClientSecret;
        }
    }
}
