using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("AuthSolution.Shared.Identify.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")] // Moq generated assembly
