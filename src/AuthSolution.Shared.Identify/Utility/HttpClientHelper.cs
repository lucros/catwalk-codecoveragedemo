﻿/*
 * Originally copied from src/Idfy.IdentityServer.Common/Utility/HttpClientHelper.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */
using System;
using System.Net.Http;
using System.Reflection;

namespace AuthSolution.Shared.Identify.Utility
{
    /// <summary>
    /// Provides a shared instance of HttpClient. Must NOT be modified - headers and properties are to be set PER REQUEST
    /// </summary>
    public static class HttpClientHelper
    {
        private static readonly FieldInfo? HttpClientDisposedMemberInfo = typeof(HttpClient)
            .GetField("_disposed", BindingFlags.Instance | BindingFlags.NonPublic);

        private static HttpClient? _current;
        private static DateTime _expires = DateTime.MinValue;

        public static TimeSpan HttpClientLifeSpan { get; set; } = TimeSpan.FromMinutes(2);

        public static HttpClient Client
        {
            get
            {
                if (_current != null && (bool?)HttpClientDisposedMemberInfo?.GetValue(_current) == true)
                {
                    throw new ObjectDisposedException(
                        "Someone used HttpClientHelper wrong! HttpClient has been disposed from outside this class");
                }

                if (_current == null || DateTime.Now > _expires)
                {
                    _current?.Dispose();
                    _current = new HttpClient();
                    _expires = DateTime.Now + HttpClientLifeSpan;
                }

                return _current;
            }

            internal set
            {
                _current = value;
                _expires = DateTime.Now + HttpClientLifeSpan;
            }
        }
    }
}
