﻿/*
 * Originally copied from src/Idfy.IdentityServer.Services.Idfy/Id/IdApiService.cs
 * of git@github.com:Signereno/Idfy.IdentityServer.git @ 674eb992313cacf22926bcf0978b047243494e97
 */
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ArgumentValidator;
using AuthSolution.Shared.Identify.Abstractions.Config;
using AuthSolution.Shared.Identify.Models;
using AuthSolution.Shared.Identify.Utility;
using Idfy.Core.Security.Http.Abstractions;
using Newtonsoft.Json;
using Serilog;

namespace AuthSolution.Shared.Identify
{
    public class IdApiService : IIdApiService
    {
        private readonly IdentifyApiConfig _options;
        private readonly ILogger _logger;
        private readonly IHttpRequestCredentialsApplier _httpRequestCredentialsApplier;

        public IdApiService(IdentifyApiConfig options, ILogger logger, IHttpRequestCredentialsApplier httpRequestCredentialsApplier)
        {
            Throw.IfNull(options.BaseUrl, nameof(options.BaseUrl));
            Throw.IfNull(options.Credentials, nameof(options.Credentials));
            _logger = logger.ForContext<IdApiService>();

            _options = options;

            options.BaseUrl = FixUrl(options.BaseUrl!);

            _httpRequestCredentialsApplier = httpRequestCredentialsApplier;
        }

        public async Task<IdSessionGetDto> CreateIdentificationRequest(IdSessionCreateDto request, string accountId)
        {
            HttpResponseMessage response = await DoRequest(
                $"/sessions?accountId={accountId}",
                HttpMethod.Post,
                req => req.Content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json")
                );

            string? responseStringBody = await response.Content.ReadAsStringAsync();
            var responseObject = JsonConvert.DeserializeObject<IdSessionGetDto>(responseStringBody);
            return responseObject;
        }

        public async Task<IdSessionGetDto> GetIdentificationResponse(string sessionId, string accountId)
        {
            HttpResponseMessage response = await DoRequest(
                $"/sessions/{sessionId}?accountId={accountId}",
                HttpMethod.Get);

            string? responseStringBody = await response.Content.ReadAsStringAsync();
            IdSessionGetDto? responseObject = JsonConvert.DeserializeObject<IdSessionGetDto>(responseStringBody);
            return responseObject;
        }

        public async Task<List<IdIdentityProviderDto>> ListIdentityProviders(string? accountId)
        {
            string path = string.IsNullOrEmpty(accountId)
                ? "/id-providers?language=none"
                : $"/id-providers/account?language=none&accountId={accountId}";

            bool applyCredentials = !string.IsNullOrEmpty(accountId);

            HttpResponseMessage response = await DoRequest(
                path,
                HttpMethod.Get,
                applyCredentials: applyCredentials);

            string? responseStringBody = await response.Content.ReadAsStringAsync();
            var responseObject = JsonConvert.DeserializeObject<List<IdIdentityProviderDto>>(responseStringBody);

            return responseObject;
        }

        internal async Task<HttpResponseMessage> DoRequest(
            string path,
            HttpMethod method,
            Action<HttpRequestMessage>? requestFn = null,
            bool applyCredentials = true)
        {
            using var req = new HttpRequestMessage(method, $"{_options.BaseUrl}{path}");

            if (applyCredentials)
            {
                await _httpRequestCredentialsApplier.ApplyCredentials(req);
            }

            req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            req.Headers.Add("x-signicat-context", "team-auth-oidc");
            requestFn?.Invoke(req);
            HttpResponseMessage? response = await HttpClientHelper.Client.SendAsync(req);

            if (!response.IsSuccessStatusCode)
            {
                Exception? innerException = null;

                string? body = null;
                try
                {
                    body = await response.Content.ReadAsStringAsync();
                }
                catch (Exception exception)
                {
                    innerException = exception;
                }

                _logger
                    .ForContext("Body", body)
                    .ForContext("RequestUri", response.RequestMessage.RequestUri)
                    .Error("Error {StatusCode} {Reason} from ID API.", response.StatusCode, response.ReasonPhrase);

                throw new InvalidOperationException($"IdApi error: {response.StatusCode} {response.ReasonPhrase}", innerException);
            }

            return response;
        }

        private static string FixUrl(string url)
        {
            url = url.Trim();
            if (url.EndsWith("/", StringComparison.Ordinal))
            {
                url = url.Remove(url.Length - 1);
            }

            return url;
        }
    }
}
