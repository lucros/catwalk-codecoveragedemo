﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AuthSolution.Shared.Identify.Models;

namespace AuthSolution.Shared.Identify
{
    public interface IIdApiService
    {
        Task<IdSessionGetDto> CreateIdentificationRequest(IdSessionCreateDto request, string accountId);

        Task<IdSessionGetDto> GetIdentificationResponse(string sessionId, string accountId);

        Task<List<IdIdentityProviderDto>> ListIdentityProviders(string? accountId);
    }
}
