# Catwalk - Code Coverage
[![pipeline status](https://gitlab.com/lucros/catwalk-codecoveragedemo/badges/master/pipeline.svg)](https://gitlab.com/lucros/catwalk-codecoveragedemo/-/pipelines?ref=master)
[![coverage report](https://gitlab.com/lucros/catwalk-codecoveragedemo/badges/master/coverage.svg)](https://gitlab.com/lucros/catwalk-codecoveragedemo/-/commits/master)

## Overview
Official repo: [Team Auth - Shared Identify](https://gitlab.com/signicat/auth/sharedgroup/identify)

* Language: **C#**
* Framework: **.NET Core** *3.1*
* Unit test framework: **XUnit** *2.4.1*

# Running the tests
You can check the results by running the following commands locally in this root folder:

1. Install the **ReportGenerator** tool:
   ```shell
   dotnet tool install --tool-path tools dotnet-reportgenerator-globaltool
   ```
   > **NOTE**
   > This command should be executed once.

1. Execute the unit tests:
   ```shell
   dotnet test -r testreports --collect:"XPlat Code Coverage" --test-adapter-path:. --logger:"junit;LogFilePath=..\..\testreports\{assembly}-test-result.xml;MethodFormat=Class;FailureBodyFormat=Verbose"
   ```

1. Generate the report through **ReportGenerator** based on unit tests results:
   ```shell
   ./tools/reportgenerator '-reports:testreports/*/coverage.cobertura.xml' -reporttypes:TextSummary -targetdir:testreports
   ```
   The summary will be available in `./testreports/Summary.txt` file.

# CI Pipeline
Same steps from [Running the tests](#running-the-tests) section are applied here.

Additionally, the code coverage output is got from a regular expression at `coverage` section of `test` job in [GitLab CI pipeline](./.gitlab-ci.yml).

## Alternatives

### Microsoft Visual Studio
- [Fine Code Coverage](https://github.com/FortuneN/FineCodeCoverage)
  ![Fine Code Coverage in action on Microsoft Visual Studio](./docs/Microsoft_Visual_Studio-Fine_Code_Coverage.png)

### JetBrains Rider
- [dotCover](https://www.jetbrains.com/dotcover/)
  ![dotCover in action on JetBrains Rider](./docs/JetBrains-Rider_dotCover.png)

# More information
For more information about code coverage in .NET Core, check the links below:

* [dotnet test](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-test)
* [Use code coverage for unit testing](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-code-coverage)
* [Measuring .NET Core Test Coverage with Coverlet](https://www.tonyranieri.com/blog/2019/07/31/Measuring-.NET-Core-Test-Coverage-with-Coverlet/)
* [Cobertura GitHub repository](https://github.com/cobertura/cobertura)
* [Coverlet GitHub repository](https://github.com/coverlet-coverage/coverlet)
* [ReportGenerator GitHub repository](https://github.com/danielpalme/ReportGenerator)
* [Coverlet GitHub repository](https://github.com/coverlet-coverage/coverlet)
